//
//  TeamsViewController.swift
//  kpis
//
//  Created by Gustavo Pirela on 5/7/18.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class TeamsViewController: UIViewController {
    
    @IBOutlet weak var teamsTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTeamsTableView()
    }

    // MARK: - Private Methods
    
    private func configureTeamsTableView() {
        teamsTable.tableFooterView = UIView()
        teamsTable.register(UITableViewCell.self, forCellReuseIdentifier: "Standard")
        teamsTable.delegate = self
        teamsTable.dataSource = self
    }
    
    private func pushTabViewController() {
        let viewController = TabBarController()
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}

// MARK: - UITableViewDataSource
extension TeamsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Standard") else {
            return UITableViewCell(style: .default, reuseIdentifier: "Standard")
        }
        
        cell.textLabel?.text = "Mockup"
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension TeamsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pushTabViewController()
    }
    
}
