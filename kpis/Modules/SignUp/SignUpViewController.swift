//
//  SignUpViewController.swift
//  kpis
//
//  Created by Gustavo Pirela on 3/7/18.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - IBActions
    
    @IBAction func pushTeamsViewController(_ sender: UIButton) {
        let viewController = TeamsViewController(nibName: "TeamsView", bundle: nil)
        navigationController?.pushViewController(viewController, animated: true)
        navigationController?.viewControllers.remove(at: 1)
    }
    

}
