//
//  SignInViewController.swift
//  kpis
//
//  Created by Gustavo Pirela on 3/7/18.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }


    // MARK: - IBActions
    
    @IBAction func pushSignUpViewController(_ sender: UIButton) {
        let viewController = SignUpViewController(nibName: "SignUpView", bundle: nil)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func pushTeamsViewController(_ sender: UIButton) {
        let viewController = TeamsViewController(nibName: "TeamsView", bundle: nil)
        navigationController?.pushViewController(viewController, animated: true)
    }

}
