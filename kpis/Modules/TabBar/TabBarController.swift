//
//  TabBarController.swift
//  kpis
//
//  Created by Gustavo Pirela on 5/7/18.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTabBar()
    }
    
    // MARK: - Private Methods
    
    private func configureTabBar() {
        let firstViewController = MembersViewController(nibName: "MembersView", bundle: nil)
        let secondViewController = DashboardViewController(nibName: "DashboardView", bundle: nil)
        firstViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .history, tag: 0)
        secondViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 1)
        viewControllers = [firstViewController, secondViewController]
    }
    
}
